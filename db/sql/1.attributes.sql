--
-- Table `attributes`
--
DROP TABLE IF EXISTS `attributes`;
CREATE TABLE `attributes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `startDate` DATETIME DEFAULT NULL,
  `endDate` DATETIME DEFAULT NULL,
  `status` ENUM ('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE'),
  `type` ENUM ('DETAILS', 'SPECS'),
  `reference` VARCHAR(150) DEFAULT NULL,
  `productId` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamps` (`createdAt`, `updatedAt`),
  KEY `activePeriod` (`startDate`, `endDate`),
  KEY `status` (`status`),
  KEY `product` (`id`, `productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

