--
-- Table `labels`
--
DROP TABLE IF EXISTS `labels`;
CREATE TABLE `labels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `startDate` DATETIME DEFAULT NULL,
  `endDate` DATETIME DEFAULT NULL,
  `status` ENUM ('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE'),
  `iso` VARCHAR(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subtitle` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` TEXT COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `text` TEXT COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `productId` INT(11) DEFAULT NULL,
  `categoryId` INT(11) DEFAULT NULL,
  `attributeId` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamps` (`createdAt`, `updatedAt`),
  KEY `activePeriod` (`startDate`, `endDate`),
  KEY `status` (`status`),
  KEY `i18n` (`iso`),
  KEY `product` (`id`, `productId`, `status`),
  KEY `category` (`id`, `categoryId`, `status`),
  KEY `attribute` (`id`, `attributeId`, `status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

