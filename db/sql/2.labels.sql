--
-- Table `products`
--
-- Clear table
TRUNCATE `labels`;
-- Add content
INSERT INTO `labels` (`id`, `createdAt`, `updatedAt`, `startDate`, `endDate`, `status`, `iso`, `title`, `subtitle`, `description`, `text`, `productId`, `categoryId`, `attributeId`) VALUES
(1, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'Piaget Altiplano Ultimate 910P', 'The thinnest hand-wound mechanical watches in the world. Manufacture Piaget 910P ultra-thin, hand-wound mechanical movement.', 'The world’s thinnest Automatic Watch', '<p>The Altiplano 38 mm watch is a unique concept, and resolutely proud of its status as one of the thinnest and most elegant mechanical timepieces in the world. Blending hand-wound calibre and external features into a single concept, the watch has been designed as a whole unit and is only 3.65 mm thick.</p><p>The culmination of over half a century of virtuoso craftsmanship in the field of ultra-thin watchmaking, a domain in which Piaget reigns supreme, this luxury watch symbolises all the excellence.</p>', 1, NULL, NULL),
(2, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'Watch', '', '', '', NULL, 1, NULL),
(3, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'PIAGET', '', 'Brand', '', NULL, NULL, 1),
(4, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', '60TH ANNIVERSARY', '', 'Collection', '', NULL, NULL, 2),
(5, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'AUTOMATIC MOVEMENT', '', 'Movement', '', NULL, NULL, 3),
(6, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', '2 YEARS', '', 'Warranty', '', NULL, NULL, 4),
(7, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'FOR MEN’S', '', 'Gender', '', NULL, NULL, 5),
(8, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'SWITZERLAND', '', 'Origin', '', NULL, NULL, 6),
(9, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'ROSE GOLD / SILVER', '', 'Metal', '', NULL, NULL, 7),
(10, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', '32.14', '', 'Metal Weight', '', NULL, NULL, 8),
(11, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'ALLIGATOR', '', 'Strape Type', '', NULL, NULL, 9),
(12, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'BLACK / BROWN', '', 'Strap Color', '', NULL, NULL, 10),
(13, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', 'ROUND', '', 'Case Shape', '', NULL, NULL, 11),
(14, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', '38 MM', '', 'Case Diameter', '', NULL, NULL, 12),
(15, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', '3.45 MM', '', 'Case Thickness', '', NULL, NULL, 13),
(16, now(), now(), NULL, NULL, 'ACTIVE', 'en-CH', '30 M / 02 ATM', '', 'Water Resistance', '', NULL, NULL, 14);