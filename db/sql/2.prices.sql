-- Clear table
TRUNCATE `prices`;
-- Add content
INSERT INTO `prices` (`id`, `createdAt`, `updatedAt`, `startDate`, `endDate`, `status`, `type`, `currency`, `value`, `discount`, `productId`) VALUES
(1, now(), now(), NULL, NULL, 'ACTIVE', 'FIX', 'CHF', 35700.00, NULL, 1);