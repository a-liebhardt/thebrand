--
-- Table `prices`
--
DROP TABLE IF EXISTS `prices`;
CREATE TABLE `prices` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `startDate` DATETIME DEFAULT NULL,
  `endDate` DATETIME DEFAULT NULL,
  `status` ENUM ('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE'),
  `type` ENUM ('FIX', 'ABO'),
  `currency` VARCHAR(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` float(24) DEFAULT NULL,
  `discount` float(24) DEFAULT NULL,
  `productId` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamps` (`createdAt`, `updatedAt`),
  KEY `activePeriod` (`startDate`, `endDate`),
  KEY `status` (`status`),
  KEY `currency` (`currency`),
  KEY `product` (`id`, `productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

