-- Clear table
TRUNCATE `attributes`;
-- Add content
INSERT INTO `attributes` (`id`, `createdAt`, `updatedAt`, `startDate`, `endDate`, `status`, `type`, `reference`, `productId`) VALUES
(1, now(), now(), NULL, NULL, 'ACTIVE', 'DETAILS', 'https://via.placeholder.com/60', 1),
(2, now(), now(), NULL, NULL, 'ACTIVE', 'DETAILS', 'https://via.placeholder.com/60', 1),
(3, now(), now(), NULL, NULL, 'ACTIVE', 'DETAILS', 'https://via.placeholder.com/60', 1),
(4, now(), now(), NULL, NULL, 'ACTIVE', 'DETAILS', 'https://via.placeholder.com/60', 1),
(5, now(), now(), NULL, NULL, 'ACTIVE', 'DETAILS', 'https://via.placeholder.com/60', 1),
(6, now(), now(), NULL, NULL, 'ACTIVE', 'DETAILS', 'https://via.placeholder.com/60', 1),
(7, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(8, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(9, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(10, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(11, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(12, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(13, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1),
(14, now(), now(), NULL, NULL, 'ACTIVE', 'SPECS', 'https://via.placeholder.com/60', 1);
