--
-- Table `productsCategories`
--
DROP TABLE IF EXISTS `products_categories`;
CREATE TABLE `products_categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryId` INT(11) DEFAULT NULL,
  `productId` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamps` (`createdAt`, `updatedAt`),
  KEY `relation` (`categoryId`, `productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

