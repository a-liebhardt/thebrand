import DataTypes, { Model } from 'sequelize';

class Product extends Model {
  static tableName = 'products';

  static schema = {
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE')
    },
    articleNo: {
      type: DataTypes.TEXT
    }
  };

  static associate(models) {
    Product.Categories = Product.belongsToMany(models.Category, {
      through: {
        model: models.ProductCategory,
        unique: false
      },
      foreignKey: 'productId',
      onDelete: 'CASCADE'
    });
    Product.Labels = Product.hasMany(models.Label, {
      foreignKey: 'productId',
      as: 'labels',
    });
    Product.Prices = Product.hasMany(models.Price, {
      foreignKey: 'productId',
      as: 'prices',
    });
    Product.Attributes = Product.hasMany(models.Attribute, {
      foreignKey: 'productId',
      as: 'attributes',
    });
  }
}

export default (sequelize) => {
  Product.init(Product.schema, {
    sequelize,
    tableName: Product.tableName,
  });

  return Product;
};
