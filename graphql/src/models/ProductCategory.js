import DataTypes, { Model } from 'sequelize';

class ProductCategory extends Model {
  static tableName = 'products_categories';

  static schema = {
    categoryId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    productId: {
      type: DataTypes.UUID,
      allowNull: false
    },
  };

  static associate(models) {
  }
}

export default (sequelize) => {
  ProductCategory.init(ProductCategory.schema, {
    sequelize,
    tableName: ProductCategory.tableName,
  });

  return ProductCategory;
};
