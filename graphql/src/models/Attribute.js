import DataTypes, { Model } from 'sequelize';

class Attribute extends Model {
  static tableName = 'attributes';

  static schema = {
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE')
    },
    type: {
      type: DataTypes.ENUM('DETAILS', 'SPECS')
    },
    reference: {
      type: DataTypes.TEXT
    },
    productId: {
      type: DataTypes.INTEGER,
    }
  };

  static associate(models) {
    Attribute.Product = Attribute.belongsTo(models.Product, {
      as: 'product'
    });
    Attribute.Labels = Attribute.hasMany(models.Label, {
      foreignKey: 'attributeId',
      as: 'labels',
    });
  }
}

export default (sequelize) => {
  Attribute.init(Attribute.schema, {
    sequelize,
    tableName: Attribute.tableName,
  });

  return Attribute;
};
