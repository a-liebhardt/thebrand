import DataTypes, { Model } from 'sequelize';

class Category extends Model {
  static tableName = 'categories';

  static schema = {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE')
    }
  };

  static associate(models) {
    Category.Products = Category.belongsToMany(models.Product, {
      through: {
        model: models.ProductCategory,
        unique: false
      },
      foreignKey: 'categoryId',
      onDelete: 'CASCADE'
    });
    Category.Labels = Category.hasMany(models.Label, {
      foreignKey: 'categoryId',
      as: 'labels',
    });
  }
}

export default (sequelize) => {
  Category.init(Category.schema, {
    sequelize,
    tableName: Category.tableName,
  });

  return Category;
};
