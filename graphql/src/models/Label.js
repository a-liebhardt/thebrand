import DataTypes, { Model } from 'sequelize';

class Label extends Model {
  static tableName = 'labels';

  static schema = {
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE')
    },
    iso: {
      type: DataTypes.TEXT
    },
    title: {
      type: DataTypes.TEXT
    },
    subtitle: {
      type: DataTypes.TEXT
    },
    description: {
      type: DataTypes.TEXT
    },
    text: {
      type: DataTypes.TEXT
    },
    productId: {
      type: DataTypes.INTEGER,
    },
    categoryId: {
      type: DataTypes.INTEGER,
    },
    attributeId: {
      type: DataTypes.INTEGER,
    }
  };

  static associate(models) {
    Label.Product = Label.belongsTo(models.Product, {
      as: 'product'
    });
    Label.Category = Label.belongsTo(models.Category, {
      as: 'category'
    });
    Label.Attribute = Label.belongsTo(models.Attribute, {
      as: 'attribute'
    });
  }
}

export default (sequelize) => {
  Label.init(Label.schema, {
    sequelize,
    tableName: Label.tableName,
  });

  return Label;
};
