import DataTypes, { Model } from 'sequelize';

class Price extends Model {
  static tableName = 'prices';

  static schema = {
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.literal("NOW()"),
      allowNull: false
    },
    startDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    endDate: {
      type: DataTypes.DATE,
      defaultValue: null,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('DELETED', 'DISABLED', 'UNCONFIRMED', 'CONFIRMED', 'INACTIVE', 'ACTIVE')
    },
    type: {
      type: DataTypes.ENUM('FIX', 'ABO')
    },
    currency: {
      type: DataTypes.TEXT
    },
    value: {
      type: DataTypes.FLOAT,
    },
    discount: {
      type: DataTypes.FLOAT,
    },
    productId: {
      type: DataTypes.INTEGER,
    }
  };

  static associate(models) {
    Price.Product = Price.belongsTo(models.Product, {
      as: 'product'
    });
  }
}

export default (sequelize) => {
  Price.init(Price.schema, {
    sequelize,
    tableName: Price.tableName,
  });

  return Price;
};
