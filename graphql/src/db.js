import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

import sequelize from './config/database';

const db = {
  sequelize,
  Sequelize,
};

const folder = `${__dirname}/models`;

fs
  .readdirSync(folder)
  .filter(file =>
    path.extname(file) === '.js',
  )
  .forEach((file) => {
    const model = sequelize.import(path.join(folder, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
  }
});

export default db;
