import {
  resolver
} from 'graphql-sequelize';
import models from '../db';

// Attribute.js
export const typeDef = `
type Attribute {
  id: ID!
  startDate: String
  endDate: String
  status: String!
  type: String!
  reference: String
  labels: [Label]
  product: Product
}

input AttributeInput {
  id: ID
  startDate: String
  endDate: String
  status: String
  type: String
  reference: String
  productId: Int
}

extend type Mutation {
  createAttribute(input: AttributeInput): Attribute
  updateAttribute(id: ID!, input: AttributeInput): Attribute
  removeAttribute(id: ID!): Attribute
}

extend type Query {
  attribute(id: ID!): Attribute
  attributes: [Attribute]
}
`;

export const resolvers = {
  Query: {
    attribute: resolver(models.Attribute),
    attributes: resolver(models.Attribute),
  },
  Attribute: {
    product: resolver(models.Attribute.Product),
    labels: resolver(models.Attribute.Labels),
  },
  Mutation: {
    createAttribute: function(root, { input }) {
      return models.Attribute.create(input)
        .then(attribute => {
          // if (input.categories) {
          //   for (let i = 0; i < input.categories.length; i++) {
          //     models.AttributeCategory.create({ 'attributeId':attribute.dataValues.id, 'categoryId':input.categories[i].categoryId });
          //   }
          // }
          return {'id': attribute.dataValues.id};
        });
    },
    updateAttribute: function(root, { id, input }) {
      return models.Attribute.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Attribute.update(input, {'where':{ 'id':id }})
              .then(attribute => {
                // if (input.categories) {
                //   models.AttributeCategory.destroy({'where':{ 'attributeId':id }});
                //   for (let i = 0; i < input.categories.length; i++) {
                //     models.AttributeCategory.create({ 'attributeId':id, 'categoryId':input.categories[i].categoryId });
                //   }
                // }
              });
            return {'id': id};
          }
          return {'id': 0};
      });
    },
    removeAttribute: function(root, { id }) {
      return models.Attribute.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Attribute.destroy({'where':{ 'id':id }});
            // models.AttributeCategory.destroy({'where':{ 'attributeId':id }});
            return {'id': id};
          }
          return {'id': 0};
      });
    },
  }
};
