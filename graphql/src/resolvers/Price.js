import {
  resolver
} from 'graphql-sequelize';
import models from '../db';

// Price.js
export const typeDef = `
type Price {
  id: ID!
  startDate: String
  endDate: String
  status: String!
  type: String!
  currency: String!
  value: Float!
  discount: Float
  product: Product
}

input PriceInput {
  id: ID
  startDate: String
  endDate: String
  status: String
  type: String
  currency: String
  value: Float
  discount: Float
  productId: Int
}

extend type Mutation {
  createPrice(input: PriceInput): Price
  updatePrice(id: ID!, input: PriceInput): Price
  removePrice(id: ID!): Price
}

extend type Query {
  price(id: ID!): Price
  prices: [Price]
}
`;

export const resolvers = {
  Query: {
    price: resolver(models.Price),
    prices: resolver(models.Price),
  },
  Price: {
    product: resolver(models.Price.Product),
  },
  Mutation: {
    createPrice: function(root, { input }) {
      return models.Price.create(input)
        .then(price => {
          // if (input.categories) {
          //   for (let i = 0; i < input.categories.length; i++) {
          //     models.PriceCategory.create({ 'priceId':price.dataValues.id, 'categoryId':input.categories[i].categoryId });
          //   }
          // }
          return {'id': price.dataValues.id};
        });
    },
    updatePrice: function(root, { id, input }) {
      return models.Price.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Price.update(input, {'where':{ 'id':id }})
              .then(price => {
                // if (input.categories) {
                //   models.PriceCategory.destroy({'where':{ 'priceId':id }});
                //   for (let i = 0; i < input.categories.length; i++) {
                //     models.PriceCategory.create({ 'priceId':id, 'categoryId':input.categories[i].categoryId });
                //   }
                // }
              });
            return {'id': id};
          }
          return {'id': 0};
      });
    },
    removePrice: function(root, { id }) {
      return models.Price.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Price.destroy({'where':{ 'id':id }});
            // models.PriceCategory.destroy({'where':{ 'priceId':id }});
            return {'id': id};
          }
          return {'id': 0};
      });
    },
  }
};
