import {
  resolver
} from 'graphql-sequelize';
import models from '../db';

// Label.js
export const typeDef = `
type Label {
  id: ID!
  startDate: String
  endDate: String
  status: String!
  iso: String!
  title: String
  subtitle: String
  description: String
  text: String
  product: Product
  category: Category
  attribute: Attribute
}

input LabelInput {
  id: ID
  startDate: String
  endDate: String
  status: String
  iso: String!
  title: String
  subtitle: String
  description: String
  text: String
  productId: Int
  categoryId: Int
  attributeId: Int
}

extend type Mutation {
  createLabel(input: LabelInput): Label
  updateLabel(id: ID!, input: LabelInput): Label
  removeLabel(id: ID!): Label
}

extend type Query {
  label(id: ID!): Label
  labels: [Label]
}
`;

export const resolvers = {
  Query: {
    label: resolver(models.Label),
    labels: resolver(models.Label),
  },
  Label: {
    product: resolver(models.Label.Product),
    category: resolver(models.Label.Category),
    attribute: resolver(models.Label.Attribute),
  },
  Mutation: {
    createLabel: function(root, { input }) {
      return models.Label.create(input)
        .then(label => {
          // if (input.categories) {
          //   for (let i = 0; i < input.categories.length; i++) {
          //     models.LabelCategory.create({ 'labelId':label.dataValues.id, 'categoryId':input.categories[i].categoryId });
          //   }
          // }
          return {'id': label.dataValues.id};
        });
    },
    updateLabel: function(root, { id, input }) {
      return models.Label.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Label.update(input, {'where':{ 'id':id }})
              .then(label => {
                // if (input.categories) {
                //   models.LabelCategory.destroy({'where':{ 'labelId':id }});
                //   for (let i = 0; i < input.categories.length; i++) {
                //     models.LabelCategory.create({ 'labelId':id, 'categoryId':input.categories[i].categoryId });
                //   }
                // }
              });
            return {'id': id};
          }
          return {'id': 0};
      });
    },
    removeLabel: function(root, { id }) {
      return models.Label.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Label.destroy({'where':{ 'id':id }});
            // models.LabelCategory.destroy({'where':{ 'labelId':id }});
            return {'id': id};
          }
          return {'id': 0};
      });
    },
  }
};
