import {
  resolver
} from 'graphql-sequelize';
import models from '../db';

// ProductCategory.js
export const typeDef = `
type ProductCategory {
  userId: Int
  roleId: Int
}

input ProductCategoryInput {
  userId: Int
  roleId: Int
}

extend type Mutation {
  createProductCategory(input: ProductCategoryInput): ProductCategory
  removeProductCategory(id: ID!): ProductCategory
}

extend type Query {
  userrole(id: ID!): ProductCategory
  userroles: [ProductCategory]
}
`;

export const resolvers = {
  Query: {
    userrole: resolver(models.ProductCategory),
    userroles: resolver(models.ProductCategory),
  },
  ProductCategory: {
    // roles: resolver(models.User.Roles),
  },
  Mutation: {
    createProductCategory: function(root, { input }) {
      return models.ProductCategory.create(input);
    },
    removeProductCategory: function(root, { userId, roleId }) {
      return models.ProductCategory.count({'where':{ 'userId':userId, 'roleId':roleId }})
        .then(count => {
          if (count > 0) {
            models.ProductCategory.destroy({'where':{ 'userId':userId, 'roleId':roleId }});
            return {'id': count};
          }
          return {'id': 0};
      });
    },
  }
};
