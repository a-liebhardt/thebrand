import {
  resolver
} from 'graphql-sequelize';
import models from '../db';

// Product.js
export const typeDef = `
type Product {
  id: ID!
  startDate: String
  endDate: String
  status: String!
  articleNo: String!
  labels: [Label]
  prices: [Price]
  attributes: [Attribute]
  categories: [Category]
}

input ProductInput {
  id: ID
  startDate: String
  endDate: String
  status: String
  articleNo: String
  categories: [ProductCategoryInput]
}

extend type Mutation {
  createProduct(input: ProductInput): Product
  updateProduct(id: ID!, input: ProductInput): Product
  removeProduct(id: ID!): Product
}

extend type Query {
  product(id: ID!): Product
  products: [Product]
}
`;

export const resolvers = {
  Query: {
    product: resolver(models.Product),
    products: resolver(models.Product),
  },
  Product: {
    labels: resolver(models.Product.Labels),
    prices: resolver(models.Product.Prices),
    attributes: resolver(models.Product.Attributes),
    categories: resolver(models.Product.Categories),
  },
  Mutation: {
    createProduct: function(root, { input }) {
      return models.Product.create(input)
        .then(product => {
          if (input.categories) {
            for (let i = 0; i < input.categories.length; i++) {
              models.ProductCategory.create({ 'productId':product.dataValues.id, 'categoryId':input.categories[i].categoryId });
            }
          }
          return {'id': product.dataValues.id};
        });
    },
    updateProduct: function(root, { id, input }) {
      return models.Product.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Product.update(input, {'where':{ 'id':id }})
              .then(product => {
                if (input.categories) {
                  models.ProductCategory.destroy({'where':{ 'productId':id }});
                  for (let i = 0; i < input.categories.length; i++) {
                    models.ProductCategory.create({ 'productId':id, 'categoryId':input.categories[i].categoryId });
                  }
                }
              });
            return {'id': id};
          }
          return {'id': 0};
      });
    },
    removeProduct: function(root, { id }) {
      return models.Product.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Product.destroy({'where':{ 'id':id }});
            models.ProductCategory.destroy({'where':{ 'productId':id }});
            return {'id': id};
          }
          return {'id': 0};
      });
    },
  }
};
