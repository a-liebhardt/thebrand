import {
  resolver
} from 'graphql-sequelize';
import models from '../db';

// Category.js
export const typeDef = `
type Category {
  id: ID!
  startDate: String
  endDate: String
  status: String!
  labels: [Label]
  products: [Product]
}

input CategoryInput {
  id: ID
  startDate: String
  endDate: String
  status: String
  products: [ProductCategoryInput]
}

extend type Mutation {
  createCategory(input: CategoryInput): Category
  updateCategory(id: ID!, input: CategoryInput): Category
  removeCategory(id: ID!): Category
}

extend type Query {
  category(id: ID!): Category
  categories: [Category]
}
`;

export const resolvers = {
  Query: {
    category: resolver(models.Category),
    categories: resolver(models.Category),
  },
  Category: {
    labels: resolver(models.Category.Labels),
    products: resolver(models.Category.Products),
  },
  Mutation: {
    createCategory: function(root, {input}) {
      return models.Category.create(input);
    },
    updateCategory: function(root, {id, input}) {
      return models.Category.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Category.update(input, {'where':{ 'id':id }});
            return {'id': id};
          }
          return {'id': 0};
      });
    },
    removeCategory: function(root, {id}) {
      return models.Category.count({'where':{ 'id':id }})
        .then(count => {
          if (count > 0) {
            models.Category.destroy({'where':{ 'id':id }});
            models.ProductCategory.destroy({'where':{ 'categoryId':id }});
            return {'id': id};
          }
          return {'id': 0};
      });
    },
  }
};
