import {
  GraphQLServer
} from 'graphql-yoga';
import {
  createContext,
  EXPECTED_OPTIONS_KEY
} from 'dataloader-sequelize';
import {
  resolver
} from 'graphql-sequelize';
import depthLimit from 'graphql-depth-limit'
import {
  merge
} from 'lodash';

// Load DB models
import models from './db';

// Load schema and resolvers [start]
import {
  typeDef as ProductDef,
  resolvers as ProductRes,
} from './resolvers/Product.js';
import {
  typeDef as CategoryDef,
  resolvers as CategoryRes,
} from './resolvers/Category.js';
import {
  typeDef as ProductCategoryDef,
  resolvers as ProductCategoryRes,
} from './resolvers/ProductCategory.js';
import {
  typeDef as LabelDef,
  resolvers as LabelRes,
} from './resolvers/Label.js';
import {
  typeDef as PriceDef,
  resolvers as PriceRes,
} from './resolvers/Price.js';
import {
  typeDef as AttributeDef,
  resolvers as AttributeRes,
} from './resolvers/Attribute';
// Load schema and resolvers [end]

const Query = `
type Query {
  _empty: String
}

type Mutation {
  _empty: String
}
`;

const resolvers = {};

// Tell `graphql-sequelize` where to find the DataLoader context in the
// global request context
resolver.contextToOptions = {
  [EXPECTED_OPTIONS_KEY]: EXPECTED_OPTIONS_KEY
};

function init() {
  // if (process.env.INTERFACE_ENV === 'development') {
  //   // Sync database
  //   models.sequelize.sync({
  //     force: true
  //   });
  // }

  const server = new GraphQLServer({
    typeDefs: [
      Query,
      ProductDef,
      CategoryDef,
      ProductCategoryDef,
      LabelDef,
      PriceDef,
      AttributeDef
    ],
    resolvers: merge(
      resolvers,
      ProductRes,
      CategoryRes,
      ProductCategoryRes,
      LabelRes,
      PriceRes,
      AttributeRes
    ),
    validationRules: [depthLimit(10, {
        ignore: [/_trusted$/, 'idontcare']
      },
      depths => console.log(depths))],
    context(req) {
      // For each request, create a DataLoader context for Sequelize to use
      const dataloaderContext = createContext(models.sequelize);

      // Using the same EXPECTED_OPTIONS_KEY, store the DataLoader context
      // in the global request context
      return {
        [EXPECTED_OPTIONS_KEY]: dataloaderContext,
      };
    },
  });

  return server;
}

export default init();
