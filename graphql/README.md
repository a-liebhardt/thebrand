# GrapQL Server

A GrapQl Server with sequalize build to run in docker as well as outside the box.

To start outside the box Node 8.9.0 or higher is required.
You also need a local database. In the current process MaraiDB is used. Please checkout docker-compose.yml for credentials.

Install server
```sh
npm build
```

Start server
```sh
npm start
```

Navigate to http://localhost:4200/

A collection of demo queries.

## User

```sh
query {
  users {
    id,
    username,
    email,
    roles {
      id,
      name,
      description
    }
  }
}
```

### by ID

```sh
query {
  user(id: <ID>) {
    key,
    value,
    character {
      id,
      name
    }
  }
}
```

### Create new

```sh
mutation {
  createUser(input: {
    username: "Lorem Ipsum",
    email: "hello@world.ch",
    roles: [
      {roleId:1},
      {roleId:3},
      {roleId:2}
    ]
  }) {
    id
  }
}
```

### Update by ID

```sh
mutation {
  updateUser(id: 2, input: {
    username: "Lorem ipsum dolores ips",
    email: "hello@world.ch",
    roles: [
      {roleId:2},
      {roleId:3}
    ]
  }) {
    id
  }
}
```

### Delete by ID
```sh
mutation {
  removeUser(id: 1) {
    id
  }
}
```

## Role

```sh
query {
  roles {
    id,
    name,
    description,
    users {
      id,
      username
    }
  }
}
```

### by ID

```sh
query {
  role(id: <ID>) {
    key,
    value,
    character {
      id,
      name
    }
  }
}
```

### Create new

```sh
mutation {
  createRole(input: {
    name: "ALL",
    description: "Role desciption"
  }) {
    id
  }
}
```

### Update by ID

```sh
mutation {
  updateRole(id: <ID>, input: {
    ...
  }) {
    id
  }
}
```

### Delete by ID
```sh
mutation {
  deleteRole(id: <ID>) {
    id
  }
}
```