# thebrand

> Docker infrastucture

![The Brand](./thebrand.png "The Brand") \
(Made with https://app.diagrams.net/)

## Build Setup

``` bash
# build docker containers
docker-compose build

# start up docker containers at localhost:8080
docker-compose up

# build and start docker containers
docker-compose up --build
```