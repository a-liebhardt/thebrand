import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/pages/HelloWorld'
import ProductDetailPage from '@/components/pages/ProductDetailPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ProductDetailPage',
      component: ProductDetailPage
    },
    {
      path: '/watches',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/jewellery',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/wedding',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/acedemy',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/company',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/stores',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})
